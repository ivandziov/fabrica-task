import datetime

from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse


class ClientTestCase(APITestCase):
    test_client = {'phone_number': '79515538493',
                   'mobile_operator_code': 951,
                   'tag': 'someTag',
                   'time_zone': 'UTC+2',
                   }
    test_client_after_update = {'phone_number': '79515538493',
                                'mobile_operator_code': 951,
                                'tag': 'tag',
                                'time_zone': 'UTC+2',
                                }
    bad_client = {'phone_number': '795538493',
                  'mobile_operator_code': 951,
                  'tag': 'someTag',
                  'time_zone': 'UTC+2',
                  }
    create_url = reverse('create_client')
    delete_url = reverse('delete_client', kwargs={'pk': 1})
    get_clients_url = reverse('get_all_clients')
    update_url = reverse('update_client', kwargs={'pk': 3})

    def test_create_client(self):
        response = self.client.post(self.create_url, data=self.test_client)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_client_with_bad_request(self):
        response = self.client.post(self.create_url, data=self.bad_client)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_client_list_view(self):
        self.client.post(self.create_url, self.test_client)
        response = self.client.get(self.get_clients_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data, [self.test_client])

    def test_client_delete(self):
        self.client.post(self.create_url, self.test_client)
        response = self.client.delete(self.delete_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_client_update(self):
        self.client.post(self.create_url, self.test_client)
        response = self.client.patch(self.update_url, data={
            'tag': 'tag',
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.test_client_after_update)
