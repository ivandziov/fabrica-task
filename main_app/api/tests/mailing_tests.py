import datetime

from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse


class MailingTestCase(APITestCase):
    test_mailing = {
        'mailing_start_time': '2022-12-08T09:11:00Z',
        'mobile_operator_code': 951,
        'tag': 'someTag',
        'text': 'someText',
        'mailing_stop_time': '2022-12-08T09:12:00Z',
    }
    test_mailing_after_update = {
        'mailing_start_time': '2022-12-08T09:11:00Z',
        'mobile_operator_code': 951,
        'tag': 'tag',
        'text': 'someText',
        'mailing_stop_time': '2022-12-08T09:12:00Z',
    }
    bad_mailing = {
        'mobile_operator_code': 951,
        'tag': 'someTag',
        'text': 'someText',
        'mailing_stop_time': datetime.datetime.now(),
    }
    create_url = reverse('create_mailing')
    delete_url = reverse('delete_mailing', kwargs={'pk': 3})
    update_url = reverse('update_mailing', kwargs={'pk': 4})
    get_mailings_url = reverse('get_all_mailings')

    def test_create_mailing(self):
        response = self.client.post(self.create_url, data=self.test_mailing)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_mailing_with_bad_request(self):
        response = self.client.post(self.create_url, data=self.bad_mailing)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_client_list_view(self):
        self.client.post(self.create_url, self.test_mailing)
        response = self.client.get(self.get_mailings_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_mailing_delete(self):
        self.client.post(self.create_url, self.test_mailing)
        response = self.client.delete(self.delete_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_mailing_update(self):
        self.client.post(self.create_url, self.test_mailing)
        response = self.client.patch(self.update_url, data={
            'tag': 'tag',
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.test_mailing_after_update)
