from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse


class StatisticsTestCase(APITestCase):
    test_mailing = {
        'mailing_start_time': '2022-12-08T09:11:00Z',
        'mobile_operator_code': 951,
        'tag': 'someTag',
        'text': 'someText',
        'mailing_stop_time': '2022-12-08T09:12:00Z',
    }

    get_all_statistics_url = reverse('get_all_statistics')
    create_mailing_url = reverse('create_mailing')
    get_statistics_about_mailing_url = reverse('get_statistics_about_mailing', kwargs={'pk': 5})

    def test_statistics_get(self):
        self.client.post(self.create_mailing_url, data=self.test_mailing)
        response = self.client.get(self.get_all_statistics_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, [{'id': 5, 'total_sent_messages': 0, 'total_unsent_messages': 0}])
        response = self.client.get(self.get_statistics_about_mailing_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'id': 5, 'total_sent_messages': 0, 'total_unsent_messages': 0})
