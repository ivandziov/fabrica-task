from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Client, Mailing
from .serializers import ClientSerializer, MailingSerializer, StatisticSerializer, \
    OneMailingStatisticSerializer


class ClientListView(generics.ListAPIView):
    queryset = Client.objects.filter()
    serializer_class = ClientSerializer


class ClientCreateView(generics.CreateAPIView):
    serializer_class = ClientSerializer


class ClientUpdateView(generics.UpdateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientDeleteView(generics.DestroyAPIView):
    queryset = Client.objects.all()


class MailingListView(generics.ListAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


class MailingCreateView(generics.CreateAPIView):
    serializer_class = MailingSerializer


class MailingUpdateView(generics.UpdateAPIView):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


class MailingDeleteView(generics.DestroyAPIView):
    queryset = Mailing.objects.all()


class StatisticsListView(generics.ListAPIView):
    queryset = Mailing.objects.all()
    serializer_class = StatisticSerializer


class OneMailingStatisticsView(APIView):

    def get(self, request, pk):
        queryset = Mailing.objects.get(pk=pk)
        serializer = OneMailingStatisticSerializer
        if not queryset:
            return Response({'Error': 'Рассылки с таким id не существует'})
        return Response(serializer(queryset).data)
