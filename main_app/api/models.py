from django.db import models
from django.core.validators import RegexValidator


class Mailing(models.Model):
    mailing_start_time = models.DateTimeField(auto_now_add=False, blank=False, null=False)
    mobile_operator_code = models.PositiveIntegerField(blank=False, null=False)
    tag = models.CharField(blank=False, null=False, max_length=100)
    text = models.CharField(max_length=1000)
    mailing_stop_time = models.DateTimeField(auto_now_add=False, blank=False, null=False)
    total_sent_messages = models.BigIntegerField(default=0, editable=False)
    total_unsent_messages = models.BigIntegerField(default=0, editable=False)


class Client(models.Model):
    phone_number_validator = RegexValidator(regex=r"^((7)+([0-9]){10})$")
    phone_number = models.CharField(null=True, validators=[phone_number_validator], max_length=11, unique=True)
    mobile_operator_code = models.PositiveIntegerField(blank=False, null=False)
    tag = models.CharField(max_length=100)
    TIME_ZONE_CHOICES = (('UTC+2', '+2:00'),
                         ('UTC+3', '+3:00'),
                         ('UTC+4', '+4:00'),
                         ('UTC+5', '+5:00'),
                         ('UTC+6', '+6:00'),
                         ('UTC+7', '+7:00'),
                         ('UTC+8', '+8:00'),
                         ('UTC+9', '+9:00'),
                         ('UTC+10', '+10:00'),
                         ('UTC+11', '+11:00'),
                         ('UTC+12', '+12:00'),
                         )
    time_zone = models.CharField(max_length=6, choices=TIME_ZONE_CHOICES)


class Message(models.Model):
    time_create = models.DateTimeField(auto_now_add=True, editable=False)
    mailing = models.ForeignKey(Mailing, on_delete=models.SET(None), null=True)
    client = models.ForeignKey(Client, on_delete=models.SET(None), null=True)
    STATUS_CHOICES = (('SENT', 'Отправлено'), ('UNSENT', 'Не отправлено'))
    status = models.CharField(max_length=13, choices=STATUS_CHOICES)
