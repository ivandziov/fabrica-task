from django.urls import path
from .views import ClientUpdateView, ClientDeleteView, ClientCreateView, MailingCreateView, MailingUpdateView, \
    MailingDeleteView, ClientListView, MailingListView, StatisticsListView, OneMailingStatisticsView

from drf_yasg import openapi
from drf_yasg.views import get_schema_view

schema_view = get_schema_view(
    openapi.Info(
        title="Api",
        default_version='v1',
        description="test api description",
    ),
    public=True
)

urlpatterns = [
    path('client/all/', ClientListView.as_view(), name='get_all_clients'),
    path('client/create', ClientCreateView.as_view(), name='create_client'),
    path('client/update/<int:pk>/', ClientUpdateView.as_view(), name='update_client'),
    path('client/delete/<int:pk>/', ClientDeleteView.as_view(), name='delete_client'),
    path('mailing/all/', MailingListView.as_view(), name='get_all_mailings'),
    path('mailing/create/', MailingCreateView.as_view(), name='create_mailing'),
    path('mailing/update/<int:pk>/', MailingUpdateView.as_view(), name='update_mailing'),
    path('mailing/delete/<int:pk>/', MailingDeleteView.as_view(), name='delete_mailing'),
    path('statistics/all/', StatisticsListView.as_view(), name='get_all_statistics'),
    path('statistics/mailing/<int:pk>/', OneMailingStatisticsView.as_view(),
         name='get_statistics_about_mailing'),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
