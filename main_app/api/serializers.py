from rest_framework import serializers
from .models import Client, Mailing, Message


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ('phone_number', 'mobile_operator_code', 'tag', 'time_zone',)


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = ('mailing_start_time', 'mobile_operator_code', 'tag', 'text', 'mailing_stop_time',)


class StatisticSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = ('id', 'total_sent_messages', 'total_unsent_messages')


class OneMailingStatisticSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = ('id', 'total_sent_messages', 'total_unsent_messages')
