import requests

from database_queries import update_message_statistic, add_message_to_database
from settings import SEND_MESSAGE_API_URL, HEADERS


def get_response_from_api(mailing, id):
    data = requests.post(url=f'{SEND_MESSAGE_API_URL}{id}', headers=HEADERS, json={
        "id": id,
        "phone": mailing['phone_number'],
        "text": mailing['text'],
    })
    data = data.json()
    return data['code']


def send_messages(mailings, cursor, connection, id):
    for mailing in mailings:
        response_code = get_response_from_api(mailing, id)
        add_message_to_database(mailing, response_code,
                                cursor, connection)
        update_message_statistic(response_code, cursor, mailing['mailing_id'], connection)
        id += 1
