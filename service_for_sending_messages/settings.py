import os
from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

HOST = os.environ.get('DB_HOST')
PASSWORD = os.environ.get('DB_PASSWORD')
PORT = os.environ.get('DB_PORT')
DATABASE = os.environ.get('DB_NAME')
USER = os.environ.get('DB_USERNAME')

HEADERS = {
    'Authorization': os.environ.get('AUTHORIZATION_TOKEN'),
}

SEND_MESSAGE_API_URL = os.environ.get('SEND_MESSAGE_API_URL')
