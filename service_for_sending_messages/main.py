import psycopg2

from database_queries import get_message_id, get_mailings_list
from requests_ import send_messages
from settings import HOST, PORT, PASSWORD, DATABASE, USER


def start_service():
    with psycopg2.connect(
            host=HOST,
            password=PASSWORD,
            port=PORT,
            database=DATABASE,
            user=USER,
    ) as connection:
        with connection.cursor() as cursor:
            while True:
                message_id = get_message_id(cursor)
                mailings_list = get_mailings_list(cursor)
                send_messages(mailings_list, cursor, connection, message_id)


if __name__ == "__main__":
    start_service()
