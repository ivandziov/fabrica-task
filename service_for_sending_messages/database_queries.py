import datetime

def get_message_id(cursor) -> int:
    id_query = """SELECT MAX(id) FROM api_message"""
    cursor.execute(id_query)
    try:
        id = cursor.fetchone()[0] + 1
    except Exception:
        id = 0
    return id


def get_mailings_list(cursor) -> list:
    query = """SELECT text, phone_number, api_client.id, api_mailing.id FROM api_mailing, api_client
                     WHERE (mailing_start_time < CURRENT_TIMESTAMP) AND (mailing_stop_time > CURRENT_TIMESTAMP) AND 
                     (api_mailing.mobile_operator_code = api_client.mobile_operator_code)
                     AND (NOT EXISTS (SELECT * FROM api_message WHERE (client_id = api_client.id
                     AND mailing_id = api_mailing.id)))"""
    cursor.execute(query)
    mailings = cursor.fetchall()
    list_of_mailings = []
    for mailing in mailings:
        list_of_mailings.append({'text': mailing[0], 'phone_number': mailing[1],
                                 'client_id': mailing[2], 'mailing_id': mailing[3]})
    return list_of_mailings


def update_message_statistic(response_code, cursor, mailing_id, connection):
    if response_code == 0:
        query = f'UPDATE api_mailing SET total_sent_messages = total_sent_messages + 1 ' \
                f'WHERE id = {mailing_id} '
    else:
        query = f'UPDATE api_mailing SET total_unsent_messages = total_unsent_messages + 1 ' \
                f'WHERE id = {mailing_id} '
    cursor.execute(query)
    connection.commit()


def add_message_to_database(mailing, response_code, cursor, connection):
    postgres_insert_query = """ INSERT INTO api_message (mailing_id, time_create, client_id, status)
     VALUES (%s, %s, %s, %s)"""
    record_to_insert = (mailing['mailing_id'], str(datetime.datetime.now()), mailing['client_id'],
                        'SENT' if response_code == 0 else 'UNSENT')
    cursor.execute(postgres_insert_query, record_to_insert)
    connection.commit()
