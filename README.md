# fabrica-task



## Запуск проекта

Для локального запуска проекта нужно:  
1. Файл fabrica-task/main_app/env переименовать в fabrica-task/main_app/.env  
В этом же файле поменять значение ENVSECRETKEY (прислал вместе с ссылкой на репозиторий) и ENVDBPASSWORD на test_password
    
2. Файл fabrica-task/main_app/env переимновать в fabrica-task/main_app/.env  
В этом же файле поменять значение EENVAUTHORIZATIONTOKEN (Вы присылали вместе с заданием) и ENVDBPASSWORD на test_password  
  
3. Прописать в терминале команду: docker-compose up --build  
  
## Документация для API
Путь для файла с документации в формате OpenApi:  fabrica-task/main_app/api/api_docs.yaml
  
## Выполненные дополнительные задания
1. Частично организовал тестирование написанного кода.  
2. Подготовил docker-compose для запуска всех сервисов проекта одной командой.  
3. Обеспечил автоматическую сборку/тестирование с помощью GitLab CI.  
4. Написал шаблоны файлов (deployment, ingress) для запуска проекта в kubernetes и отдельную job в gitlabCI для деплоя.  
Чтобы применить к работающему кластеру необходимо больше информации kubeconfig, namespace, domainname, db url and creds  
5. По адресу /api/docs открывается страница со Swagger UI.







